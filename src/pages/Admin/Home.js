import React, { Component } from 'react';
import {
	Table,
	Button,
	Input,
	Row,
	Col,
	Container,
	FormGroup,
	Pagination,
	PaginationItem,
	PaginationLink
} from 'reactstrap';
import './admin_home.css';
import { get, post } from '../../service/service';
import moment from 'moment';
import swal from 'sweetalert';
import XlsExport from 'xlsexport';
import { async } from 'q';

export default class Home extends Component {
	constructor(props) {
		super(props);

		this.state = {
			dataAll: [],
			status: 1,
			currentPage: 0
		};
	}
	componentDidMount() {
		let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {
			let th = require('moment/locale/th');
			moment.updateLocale('th', th);
			this.getdataAll(1);
		}
	}
	async getdataAll(e) {
		let check = Number(e);
		let repair = await get('/getrepair');
		let request = await get('/getrequest');
		let data = repair.result.concat(request.result);
		this.setState({
			dataAll: data
				.sort((a, b) => moment(b.date) - moment(a.date))
				.map((e, i) => {
					let date = moment(e.date).format('DD MMMM') + ' ' + (Number(moment(e.date).format('YYYY')) + 543);
					let time = moment(e.date).format('HH:mm');
					let cname = 'scb' + e.cname;
					return {
						id: e.rpid || e.rqid,
						date,
						time,
						cname,
						type: e.type,
						name: e.fname + ' ' + e.lname,
						status: e.status,
						amount: e.amount || '-'
					};
				})
				.filter((e) => e.status == (check == 1 ? 'รอดำเนินการ' : check == 2 ? 'กำลังดำเนินการ' : 'เสร็จสิ้น')),
			status: check
		});
	}
	searchText(e) {
		let { status } = this.state;
		this.getdataAll(status).then(() => {
			let { dataAll } = this.state;
			let texts = e.toLowerCase();
			let a = dataAll.filter(
				(el) =>
					el.date.toLowerCase().indexOf(texts) > -1 ||
					el.time.toLowerCase().indexOf(texts) > -1 ||
					el.cname.toLowerCase().indexOf(texts) > -1 ||
					el.type.toLowerCase().indexOf(texts) > -1 ||
					el.name.toLowerCase().indexOf(texts) > -1 ||
					el.status.toLowerCase().indexOf(texts) > -1 ||
					String(el.amount).toLowerCase().indexOf(texts) > -1
			);
			this.setState({ dataAll: a, currentPage: 0 });
		});
	}
	async delrepair(e, t) {
		swal({
			title: 'Are you sure?',
			text: 'คุณต้องการลบรายการไหม?',
			icon: 'warning',
			buttons: true,
			dangerMode: true
		}).then(async (willDelete) => {
			if (willDelete) {
				let res = await post(t == 'แจ้งซ่อม' ? '/userdelrepair' : '/userdelrequest', { id: e });
				if (res.success) {
					swal('สำเร็จ!', res.message, 'success', {
						buttons: false,
						timer: 1500
					}).then(() => {
						window.location.reload();
					});
				} else {
					swal('ผิดพลาด!', 'ลบไม่สำเร็จ', 'error', {
						buttons: false,
						timer: 1500
					});
				}
			}
		});
	}
	exportXsl = () => {
		let { dataAll } = this.state;
		let download_xls = dataAll.map((e) => ({
			ลำดับ: e.id,
			วันที่: e.date,
			เวลา: e.time,
			สถานที่: e.cname,
			ประเภท: e.type,
			ชื่อผู้แจ้งปัญหา: e.name,
			สถานะ: e.status,
			จำนวน: e.amount
		}));
		// console.log('download_xls :', download_xls);
		if (download_xls.length > 0) {
			const xls = new XlsExport(download_xls, 'รายการเสร็จสิ้น');
			xls.exportToXLS(
				'รายการเสร็จสิ้น' +
					'(' +
					moment(new Date()).format('DD MMMM') +
					' ' +
					(Number(moment(new Date()).format('YYYY')) + 543) +
					')' +
					'.xls'
			);
		}
	};
	handleClick(e, index) {
		e.preventDefault();
		this.setState({
			currentPage: index
		});
	}
	render() {
		let { dataAll, status, currentPage } = this.state;
		let pageSize = 5;
		let pagesCount = Math.ceil(dataAll.length > 0 && dataAll.length / pageSize);
		return (
			<div id="formadmin" style={{ backgroundColor: '#f8f8f8' }}>
				<Container style={{ paddingTop: '2rem' }}>
					<Row id="admincontainer" style={{ width: '90%' }}>
						<Col sm={6}>
							<Row>
								<Col sm={6}>
									<FormGroup>
										<Input type="select" onChange={(e) => this.getdataAll(e.target.value)}>
											<option value={1}>รอดำเนินการ</option>
											<option value={2}>กำลังดำเนินการ</option>
											<option value={3}>เสร็จสิ้นการดำเนินการ</option>
										</Input>
									</FormGroup>
								</Col>
							</Row>
						</Col>
						<Col sm={6}>
							<Row>
								<Col sm={6} style={{ textAlign: 'center' }}>
									<Input
										type="search"
										placeholder="search placeholder"
										onChange={(e) => this.searchText(e.target.value)}
									/>
								</Col>
								<Col sm={6} style={{ textAlign: 'center' }}>
									<Button
										disabled={
											dataAll.length > 0 &&
											dataAll.some(
												(e) => e.status === 'รอดำเนินการ' || e.status === 'กำลังดำเนินการ'
											)
										}
										color="success"
										style={{
											height: '36px',
											padding: '2px 2px 2px 2px',
											width: '130px'
										}}
										onClick={() => this.exportXsl()}
									>
										export to crv
									</Button>
								</Col>
							</Row>
						</Col>

						<Col sm={12}>
							<div>
								<Table striped responsive>
									<thead>
										<tr>
											<th>วันที่</th>
											<th>เวลา</th>
											<th>ประเภท</th>
											<th>สถานที่</th>
											<th>ชื่อผู้แจ้งปัญหา</th>
											<th>สถานะ</th>
											<th>จำนวน</th>
											<th />
										</tr>
									</thead>
									<tbody>
										{dataAll.length > 0 &&
											dataAll
												.slice(currentPage * pageSize, (currentPage + 1) * pageSize)
												.map((e) => (
													<tr>
														<td>{e.date}</td>
														<td>{e.time}</td>
														<td>{e.type}</td>
														<td>{e.cname}</td>
														<td>{e.name}</td>
														<td
															style={
																e.status == 'รอดำเนินการ' ? (
																	{ color: '#3893f5' }
																) : e.status == 'เสร็จสิ้น' ? (
																	{ color: 'green' }
																) : e.status == 'กำลังดำเนินการ' ? (
																	{ color: 'blue' }
																) : null
															}
														>
															{e.status}
														</td>
														<td>{e.amount}</td>
														<td>
															<Button
																color="success"
																style={{
																	width: '120px',
																	height: '30px',
																	padding: '2px 2px 2px 2px'
																}}
																href={
																	(e.status == 'รอดำเนินการ'
																		? '/detailWork/'
																		: '/man_re3/') +
																	(e.type == 'แจ้งซ่อม' ? 1 : 2) +
																	'/' +
																	e.id
																}
															>
																รายละเอียด
															</Button>
															{e.status == 'รอดำเนินการ' && (
																<Button
																	color="danger"
																	style={{
																		marginLeft: '1rem',
																		width: '100px',
																		height: '30px',
																		padding: '2px 2px 2px 2px'
																	}}
																	onClick={() => this.delrepair(e.id, e.type)}
																>
																	ลบ
																</Button>
															)}
														</td>
													</tr>
												))}
									</tbody>
								</Table>
							</div>
						</Col>
						<Col sm={12}>
							<br />
							<div
								className="pagination-wrapper"
								style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
							>
								<Pagination aria-label="Page navigation example">
									<PaginationItem disabled={currentPage <= 0}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage - 1)}
											previous
											href="#"
										/>
									</PaginationItem>

									{[ ...Array(pagesCount) ].map((page, i) => (
										<PaginationItem active={i === currentPage} key={i}>
											<PaginationLink onClick={(e) => this.handleClick(e, i)} href="#">
												{i + 1}
											</PaginationLink>
										</PaginationItem>
									))}

									<PaginationItem disabled={currentPage >= pagesCount - 1}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage + 1)}
											next
											href="#"
										/>
									</PaginationItem>
								</Pagination>
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}
