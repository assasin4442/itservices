import React, { Component } from 'react';
import {
	Table,
	Input,
	Row,
	Col,
	Container,
	FormGroup,
	CardImg,
	Pagination,
	PaginationItem,
	PaginationLink
} from 'reactstrap';
import { get, post, sever } from '../service/service';
import moment from 'moment';
import swal from 'sweetalert';
import { async } from 'q';
// let api = sever+'/image/tool/';
let api = sever + '/image/tool/';

export default class man_ac1 extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectRe: 1,
			datatool: [],
			dataDraw: [],
			currentPage: 0
		};
	}
	async componentDidMount() {
		let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {
			let th = require('moment/locale/th');
			moment.updateLocale('th', th);
			this.gettools();
		}
	}
	gettools = async () => {
		let res = await get('/gettools');
		this.setState({
			datatool: res.result.map((e) => {
				let type = e.type == 'tool' ? 'อุปกรณ์' : 'อะไหล่';
				return {
					id: e.id,
					type: type,
					tname: e.tname,
					amount: String(e.amount)
				};
			})
		});
	};
	getdraw = async () => {
		let USER = JSON.parse(localStorage.getItem('myData'));
		let res = await post('/getdraw_repair_manid', { id: USER.id });
		let ress = await post('/getdraw_request_manid', { id: USER.id });
		this.setState({
			dataDraw: res.result.concat(ress.result).sort((a, b) => moment(b.date) - moment(a.date)).map((e) => {
				let date = moment(e.date).format('DD MMMM') + ' ' + (Number(moment(e.date).format('YYYY')) + 543);
				let type = e.type == 'tool' ? 'อุปกรณ์' : 'อะไหล่';
				return {
					date: date,
					type: type,
					tname: e.tname,
					amount: String(e.amount)
				};
			})
		});
	};
	searchText(e) {
		let { selectRe } = this.state;
		if (selectRe == 1) {
			this.gettools().then(() => {
				let { datatool } = this.state;
				let texts = e.toLowerCase();
				let a = datatool.filter(
					(el) =>
						el.tname.toLowerCase().indexOf(texts) > -1 ||
						el.type.toLowerCase().indexOf(texts) > -1 ||
						el.amount.toLowerCase().indexOf(texts) > -1
				);
				// console.log('data', a);
				this.setState({ datatool: a, currentPage: 0 });
			});
		} else {
			this.getdraw().then(() => {
				let { dataDraw } = this.state;
				let texts = e.toLowerCase();
				let a = dataDraw.filter(
					(el) =>
						el.date.toLowerCase().indexOf(texts) > -1 ||
						el.type.toLowerCase().indexOf(texts) > -1 ||
						el.tname.toLowerCase().indexOf(texts) > -1 ||
						el.amount.toLowerCase().indexOf(texts) > -1
				);
				// console.log('data', a);
				this.setState({ dataDraw: a, currentPage: 0 });
			});
		}
	}
	handleClick(e, index) {
		e.preventDefault();
		this.setState({
			currentPage: index
		});
	}
	render() {
		let { selectRe, datatool, dataDraw, currentPage } = this.state;
		// console.log('datatool', datatool);
		let pageSize = 5;
		let pagesCount =
			selectRe == 1
				? Math.ceil(datatool.length > 0 && datatool.length / pageSize)
				: Math.ceil(dataDraw.length > 0 && dataDraw.length / pageSize);
		return (
			<div id="form" style={{ backgroundColor: '#f8f8f8' }}>
				<Container style={{ paddingTop: '3rem' }}>
					<Row id="container1" style={{ width: '90%' }}>
						<Col xs={12} sm={6}>
							<FormGroup>
								<Input
									type="select"
									name="select"
									value={selectRe}
									onChange={(e) =>
										this.setState({ selectRe: e.target.value }, () => {
											let { selectRe } = this.state;
											if (selectRe == 1) {
												this.gettools();
											} else {
												this.getdraw();
											}
										})}
								>
									<option value={1}>อะไหล่และอุปกรณ์</option>
									<option value={2}>ประวัติการเบิก</option>
								</Input>
							</FormGroup>
						</Col>

						<Col xs={12} sm={6}>
							<Input
								type="search"
								placeholder="search "
								onChange={(e) => this.searchText(e.target.value)}
							/>
						</Col>
						{selectRe == 1 ? (
							datatool.length > 0 && (
								<Col sm={12}>
									<div style={{ paddingTop: '1rem' }}>
										<Table striped responsive>
											<thead>
												<tr>
													<th>รูปภาพ</th>
													<th>ประเภท</th>
													<th>ชื่ออะไหล่และอุปกรณ์</th>
													<th>จำนวนคงเหลือ</th>
												</tr>
											</thead>
											<tbody>
												{datatool
													.slice(currentPage * pageSize, (currentPage + 1) * pageSize)
													.map((e) => (
														<tr>
															<td>
																<CardImg
																	style={{ width: 'auto', height: '3rem' }}
																	src={api + e.id + '.png'}
																/>
															</td>
															<td>{e.type}</td>
															<td>{e.tname}</td>
															<td>{e.amount}</td>
														</tr>
													))}
											</tbody>
										</Table>
									</div>
								</Col>
							)
						) : (
							dataDraw.length > 0 && (
								<Col sm={12}>
									<div style={{ paddingTop: '1rem' }}>
										<Table striped responsive>
											<thead>
												<tr>
													<th>วันที่</th>
													<th>ประเภท</th>
													<th>ชื่ออะไหล่และอุปกรณ์</th>
													<th>จำนวน</th>
												</tr>
											</thead>
											<tbody>
												{dataDraw
													.slice(currentPage * pageSize, (currentPage + 1) * pageSize)
													.map((e) => (
														<tr>
															<td>{e.date}</td>
															<td>{e.type}</td>
															<td>{e.tname}</td>
															<td>{e.amount}</td>
														</tr>
													))}
											</tbody>
										</Table>
									</div>
								</Col>
							)
						)}
						<Col sm={12}>
							<br />
							<div
								className="pagination-wrapper"
								style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
							>
								<Pagination aria-label="Page navigation example">
									<PaginationItem disabled={currentPage <= 0}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage - 1)}
											previous
											href="#"
										/>
									</PaginationItem>

									{[ ...Array(pagesCount) ].map((page, i) => (
										<PaginationItem active={i === currentPage} key={i}>
											<PaginationLink onClick={(e) => this.handleClick(e, i)} href="#">
												{i + 1}
											</PaginationLink>
										</PaginationItem>
									))}

									<PaginationItem disabled={currentPage >= pagesCount - 1}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage + 1)}
											next
											href="#"
										/>
									</PaginationItem>
								</Pagination>
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}
