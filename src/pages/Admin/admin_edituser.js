import React, { Component } from 'react';
import { Container, Row, Col, Button, FormGroup, Label, Input, Alert } from 'reactstrap';
import './adduser.css';
export default class add_user extends Component {
	render() {
		return (
			<div id="aaa" style={{ backgroundColor: '#f8f8f8' }}>
				<Container id="eee">
					<Row style={{ width: '90%' }}>
						<Col xs={12} xl={6}>
							<div id="bbb">
								<Alert color="info">แก้ไขข้อมูลผู้ใช้งาน</Alert>

								<hr />
								<Row>
									<Col>
										<Label>เลือกประเภทผู้ใช้งาน</Label>
										<Input type="select" style={{ height: '1.8rem', padding: '2px 2px 2px 2px' }}>
											<option>บุคลากร</option>
											<option>ช่างซ่อมบำรุง</option>
										</Input>
									</Col>
								</Row>
								<Row style={{ paddingTop: '12px' }}>
									<Col xs={12} sm={6}>
										<FormGroup>
											<Label>ชื่อ-สกุล</Label>
											<Input type="text" id="ccc" />
										</FormGroup>
									</Col>
									<Col xs={12} sm={6}>
										<FormGroup>
											<Label>ตำแหน่ง</Label>
											<Input type="text" id="ccc" />
										</FormGroup>
									</Col>
								</Row>
								<Row>
									<Col xs={12} sm={6}>
										<FormGroup>
											<Label>สาขาวิชา</Label>
											<Input type="text" id="ccc" />
										</FormGroup>
									</Col>
									<Col xs={12} sm={6}>
										<FormGroup>
											<Label>โทรศัพท์</Label>
											<Input type="text" id="ccc" />
										</FormGroup>
									</Col>
								</Row>
								<FormGroup>
									<Label>Email</Label>
									<Input type="text" id="ccc" />
								</FormGroup>
								<FormGroup>
									<Label>Line token</Label>
									<Input type="text" id="ccc" />
								</FormGroup>
							</div>
						</Col>
						<Col xs={12} xl={6}>
							<div id="bbb" style={{ height: '100%' }}>
								<Alert color="info">แก้ไขชื่อผู้ใช้งานและรหัสผ่าน</Alert>
								<hr />
								<Row>
									<Col>
										<Label>ชื่อผู้ใช้งาน</Label>
										<Input type="text" id="ccc" />
									</Col>
								</Row>
								<Row style={{ paddingTop: '12px' }}>
									<Col>
										<FormGroup>
											<Label>รหัสผ่าน</Label>
											<Input type="text" id="ccc" />
										</FormGroup>
									</Col>
									<Col>
										<FormGroup>
											<Label>ยืนยันรหัสผ่าน</Label>
											<Input type="text" id="ccc" />
										</FormGroup>
									</Col>
								</Row>
							</div>
						</Col>
						<div id="ddd">
							<Row>
								<Col xs={6} style={{ textAlign: 'right' }}>
									<Button color="danger" href="/admin_add" style={{ width: '8rem' }}>
										ยกเลิก
									</Button>
								</Col>
								<Col xs={6}>
									<Button color="success" style={{ width: '8rem' }}>
										ยืนยัน
									</Button>
								</Col>
							</Row>
						</div>
					</Row>
				</Container>
			</div>
		);
	}
}
