import React, { Component } from 'react';
import {
	Button,
	Label,
	Row,
	Col,
	Container,
	Alert,
	Modal,
	ModalBody,
	ModalHeader,
	CardImg,
	Table,
	Card,
	Navbar,
	NavbarBrand,
	Nav,
	NavItem,
	NavLink
} from 'reactstrap';
import './detail.css';
import { get, post,sever } from '../service/service';
import moment from 'moment';
import swal from 'sweetalert';
// let api = sever+'/image/repair/';
let api = sever+'/image/repair/';

export default class detailWork_homepage extends Component {
	constructor(props) {
		super(props);

		this.state = {
			dataRepair: [],
			dataAll: [],
			role: '',
			dataDraw: []
		};
	}

	async componentDidMount() {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		let type = this.props.match.params.type;
		if (type == 1) {
			this.getdataRepair();
			this.getDraw_repair();
		} else {
			this.getdataRequest();
			this.getDraw_request();
		}
	}
	getdataRepair = async () => {
		let res = await post('/getrepair_by_id', { id: this.props.match.params.id }); // ส่งข้อมูลไปยัง backend เพื่อเรียกใช้คำสั่ง getrepair โดยมีตัวแปลคือ id = รหัสผู้ใช้งาน และ
		// console.log('res', res);
		this.setState({
			// state ค่าที่ถูก  set ไว้เฉพาะ หน้าถ้าเปลี่ยนหน้าค่าที่ถูก set ไว้ก็จะหายไป
			dataAll: res.result.map((e, i) => {
				let date = moment(e.date).format('DD MMMM') + ' ' + (Number(moment(e.date).format('YYYY')) + 543);
				let time = moment(e.date).format('HH:mm');
				let cname = 'scb' + e.cname;
				return {
					rpid: e.rpid,
					date,
					time,
					cname,
					device_problem: e.device_problem,
					detail: e.detail,
					status: e.status,
					type: e.type,
					name: e.fname + ' ' + e.lname,
					position: e.position,
					branch: e.branch,
					phone: e.phone,
					detail: e.detail,
					device_problem: e.device_problem
				};
			})
		});
	};
	getdataRequest = async () => {
		let res = await post('/getrequest_by_id', { id: this.props.match.params.id });
		// console.log('res', res);
		this.setState({
			dataAll: res.result.map((e, i) => {
				// dataAll = ชื่อตัวแปร
				let date = moment(e.date).format('DD MMMM') + ' ' + (Number(moment(e.date).format('YYYY')) + 543);
				let time = moment(e.date).format('HH:mm');
				let cname = 'scb' + e.cname;
				return {
					rqid: e.rqid,
					date,
					time,
					cname,
					device_problem: e.device_problem,
					detail: e.detail,
					status: e.status,
					amount: e.amount,
					type: e.type,
					name: e.fname + ' ' + e.lname,
					position: e.position,
					branch: e.branch,
					phone: e.phone,
					detail: e.detail,
					device_problem: e.device_problem
				};
			})
		});
	};
	getDraw_repair = async () => {
		let res = await post('/getdraw_repair', { did: this.props.match.params.id });
		this.setState({
			dataDraw: res.result.sort((a, b) => Number(a.tid) - Number(b.tid))
		});
	};
	getDraw_request = async () => {
		let res = await post('/getdraw_request', { did: this.props.match.params.id });
		this.setState({
			dataDraw: res.result.sort((a, b) => Number(a.tid) - Number(b.tid))
		});
	};
	updateStatus = async () => {
		let type = this.props.match.params.type;
		let USER = JSON.parse(localStorage.getItem('myData'));
		if (type == 1) {
			let id = this.props.match.params.id;
			let res = await post('/updatestatus', { id, mid: USER.id });
			if (res.success) {
				swal('สำเร็จ!', res.message, 'success', {
					buttons: false,
					timer: 1500
				}).then(() => {
					this.props.history.push('/man_home1');
				});
			} else {
				swal('ผิดพลาด!', 'ทำรายการไม่สำเร็จ', 'error', {
					buttons: false,
					timer: 1500
				});
			}
		} else {
			let id = this.props.match.params.id;
			let res = await post('/updatestatus3', { id, mid: USER.id });
			if (res.success) {
				swal('สำเร็จ!', res.message, 'success', {
					buttons: false,
					timer: 1500
				}).then(() => {
					this.props.history.push('/man_home1');
				});
			} else {
				swal('ผิดพลาด!', 'ทำรายการไม่สำเร็จ', 'error', {
					buttons: false,
					timer: 1500
				});
			}
		}
	};
	render() {
		let { type, id } = this.props.match.params;
		let { dataAll, role, dataDraw } = this.state;
		return (
			<div
				id="form12"
				style={{
					backgroundColor: '#f8f8f8'
				}}
			>
				<Navbar light expand="md" style={{ backgroundColor: ' #6699cc' }}>
					<NavbarBrand href="/" style={{ fontSize: '1.50rem', fontWeight: '700', color: 'white' }}>
						IT services
					</NavbarBrand>

					<Nav className="ml-auto" navbar>
						<NavItem>
							<NavLink href="/login" style={{ color: 'white', border: '5px' }}>
								Login
							</NavLink>
						</NavItem>
						{/* <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  เข้าสูระบบ
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>Option 1</DropdownItem>
                  <DropdownItem>Option 2</DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>Reset</DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown> */}
					</Nav>
				</Navbar>
				<Container
					style={{
						paddingTop: '0.5rem'
					}}
				>
					{dataAll.length > 0 &&
						dataAll.map((e) => (
							<Row
								xs={12}
								sm={6}
								style={{
									width: '90%',
									backgroundColor: 'white',
									padding: '0.30rem 0.30rem 0.30rem 0.30rem',
									borderRadius: '1rem'
								}}
							>
								<Col xs={12} sm={6}>
									<Col
										xs={12}
										sm={12}
										style={{ backgroundColor: 'white', padding: '2px 2px 2px 2px' }}
									>
										<Alert color="info">รายการเบิกอุปกรณ์</Alert>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>วันที่</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{e.date}
													</Label>
												</Col>
											</Col>
											<Col sm={6}>
												<Col>
													<Label>เวลา</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.time}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>สถานะ</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.status}
													</Label>
												</Col>
											</Col>

											<Col sm={6}>
												<Col>
													<Label>ประเภท</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{e.type}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>สถานที่</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.cname}
													</Label>
												</Col>
											</Col>
										</Row>
										<hr />
										<Row>
											<Col sm={6}>
												<Col>
													<Label>คำขออุปกรณ์</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{type == 1 ? '-' : e.device_problem}
													</Label>
												</Col>
											</Col>

											<Col sm={6}>
												<Col>
													<Label>จำนวณ</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{type == 1 ? '-' : e.amount}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>อุปกรณ์ที่เกิดปัญหา</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{type == 1 ? e.device_problem : '-'}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>รายละเอียดงาน</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															padding: '0.5rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.detail}
													</Label>
												</Col>
											</Col>
										</Row>
									</Col>
								</Col>
								<Col xs={12} sm={6}>
									<Col xs={12} style={{ backgroundColor: 'white', padding: '2px 0px 0px 2px' }}>
										<Alert color="info">ข้อมูลผู้ใช้งาน</Alert>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>ชื่อ-สกุล</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.name}
													</Label>
												</Col>
											</Col>
											<Col sm={6}>
												<Col>
													<Label>ตำแหน่ง</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{e.position}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>สาขาวิชา</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.branch}
													</Label>
												</Col>
											</Col>

											<Col sm={6}>
												<Col>
													<Label>โทรศัพท์</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{e.phone}
													</Label>
												</Col>
											</Col>
										</Row>
									</Col>
									<Alert color="info">รูปภาพ</Alert>
									<Col>
										<Card inverse style={{ textAlign: 'center' }}>
											{type == 1 ? (
												<CardImg
													style={{ width: 'auto', height: '13rem' }}
													src={api + id + '.png'}
												/>
											) : (
												<CardImg
													style={{ width: 'auto', height: '13rem' }}
													src={require('../assets/image/default-image.jpg')}
												/>
											)}
										</Card>
									</Col>
								</Col>
								<div id="btn456">
									<Row>
										<Col
											xs={6}
											style={{
												textAlign: 'right'
											}}
										>
											<Button
												color="danger"
												style={{
													width: '8rem'
												}}
												onClick={() => this.props.history.goBack()}
											>
												กลับ
											</Button>
										</Col>
									</Row>
								</div>
							</Row>
						))}
				</Container>
			</div>
		);
	}
}
