import React, { Component } from 'react';

import {
	Row,
	Col,
	Container,
	Input,
	Button,
	Table,
	FormGroup,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Label,
	UncontrolledButtonDropdown,
	DropdownMenu,
	DropdownItem,
	DropdownToggle,
	Alert,
	Pagination,
	PaginationItem,
	PaginationLink
} from 'reactstrap';
import { get, post } from '../../service/service';
import moment from 'moment';
import swal from 'sweetalert';
import XlsExport from 'xlsexport';
import './add_man_user.css';
import { async } from 'q';

export default class add_user_man extends Component {
	constructor(props) {
		super(props);

		this.state = {
			datauser: [],
			currentPage: 0
		};
	}
	componentDidMount() {
		let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {
			let th = require('moment/locale/th');
			moment.updateLocale('th', th);
			this.getuser();
		}
	}
	getuser = async () => {
		let res = await get('/user_all');
		console.log('res', res);
		this.setState({
			datauser: res.result.filter((e) => e.role == 'admin' || e.role == 'man' || e.role == 'user').map((e) => {
				let role = e.role == 'admin' ? 'ผู้ดูแลระบบ' : e.role == 'man' ? 'ช่างซ่อมบำรุง' : 'บุคลากร';
				let name = e.prefix + e.fname + ' ' + e.lname;
				return {
					id: e.id,
					phone: e.phone,
					email: e.email,
					role: role,
					name: name,
					username: e.username,
					position: e.position
				};
			})
		});
	};
	searchText(e) {
		this.getuser().then(() => {
			let { datauser } = this.state;
			let texts = e.toLowerCase();
			let a = datauser.filter(
				(el) =>
					String(el.phone).toLowerCase().indexOf(texts) > -1 ||
					String(el.email).toLowerCase().indexOf(texts) > -1 ||
					String(el.role).toLowerCase().indexOf(texts) > -1 ||
					String(el.name).toLowerCase().indexOf(texts) > -1 ||
					String(el.username).toLowerCase().indexOf(texts) > -1 ||
					String(el.position).toLowerCase().indexOf(texts) > -1
			);
			this.setState({ datauser: a, currentPage: 0 });
		});
	}
	del_user = async (e) => {
		// alert(e);
		swal({
			title: 'Are you sure?',
			text: 'คุณต้องการทำรายการต่อหรือไหม?',
			icon: 'warning',
			buttons: true,
			dangerMode: true
		}).then(async (willDelete) => {
			if (willDelete) {
				let res = await post('/del_user', { id: e });
				if (res.success) {
					swal('สำเร็จ!', res.message, 'success').then(() => this.getuser());
				} else {
					swal('ผิดพลาด!', 'ลบไม่สำเร็จ', 'error', {
						buttons: false,
						timer: 1500
					});
				}
			}
		});
	};
	exportXsl = () => {
		let { datauser } = this.state;
		let download_xls = datauser;
		// console.log('download_xls :', download_xls);
		if (download_xls.length > 0) {
			const xls = new XlsExport(download_xls, 'รายชื่อผู้ใช้งาน');
			xls.exportToXLS(
				'รายชื่อผู้ใช้งาน' +
					'(' +
					moment(new Date()).format('DD MMMM') +
					' ' +
					(Number(moment(new Date()).format('YYYY')) + 543) +
					')' +
					'.xls'
			);
		}
	};
	handleClick(e, index) {
		e.preventDefault();
		this.setState({
			currentPage: index
		});
	}
	render() {
		let { datauser, currentPage } = this.state;
		let pageSize = 5;
		let pagesCount = Math.ceil(datauser.length > 0 && datauser.length / pageSize);
		return (
			<div id="formadduser" style={{ background: '#f8f8f8' }}>
				<Container style={{ paddingTop: '2rem' }}>
					<Row
						id="addcontainer"
						style={{
							width: '90%'
						}}
					>
						<Col sm={6}>
							<Row>
								<Col sm={12} style={{ display: 'flex', justifyContent: 'row' }}>
									<Button
										color="info"
										href="/add_user"
										style={{
											width: '150px',
											height: '36px'
										}}
									>
										เพิ่มผู้ใช้งาน
									</Button>
								</Col>
							</Row>
						</Col>
						<Col sm={6}>
							<Row>
								<Col sm={6}>
									<Input
										type="search"
										onChange={(e) => this.searchText(e.target.value)}
										placeholder="search placeholder"
									/>
								</Col>
								<Col sm={6} style={{ textAlign: 'center' }}>
									<Button
										color="success"
										style={{
											height: '36px',
											padding: '2px 2px 2px 2px',
											width: '130px'
										}}
										onClick={() => this.exportXsl()}
									>
										export to crv
									</Button>
								</Col>
							</Row>
						</Col>

						<Col sm={12} style={{ textAlign: 'center', marginTop: '1rem' }}>
							<div>
								<Table striped responsive>
									<thead>
										<tr>
											<th>ประเภท</th>
											<th>ชื่อผู้ใช้งาน</th>
											<th>ชื่อ สกุล</th>
											<th>ตำแหน่ง</th>
											<th>เบอร์โทร</th>
											<th>อีเมลล์</th>
											<th>จัดการ</th>
										</tr>
									</thead>
									<tbody>
										{datauser.length > 0 &&
											datauser
												.slice(currentPage * pageSize, (currentPage + 1) * pageSize)
												.map((e) => (
													<tr>
														<td>{e.role}</td>
														<td>{e.username}</td>
														<td>{e.name} </td>
														<td>{e.position} </td>
														<td>{e.phone} </td>
														<td>{e.email} </td>
														<td>
															<Button
																style={{ marginRight: '0.5rem' }}
																size="sm"
																color="info"
																href={'/add_user/' + e.id}
															>
																แก้ไข
															</Button>
															<Button
																disabled={e.role === 'ผู้ดูแลระบบ'}
																size="sm"
																color="danger"
																onClick={() => this.del_user(e.id)}
															>
																ลบ
															</Button>
														</td>
													</tr>
												))}
									</tbody>
								</Table>
							</div>
						</Col>
						<Col sm={12}>
							<br />
							<div
								className="pagination-wrapper"
								style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
							>
								<Pagination aria-label="Page navigation example">
									<PaginationItem disabled={currentPage <= 0}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage - 1)}
											previous
											href="#"
										/>
									</PaginationItem>

									{[ ...Array(pagesCount) ].map((page, i) => (
										<PaginationItem active={i === currentPage} key={i}>
											<PaginationLink onClick={(e) => this.handleClick(e, i)} href="#">
												{i + 1}
											</PaginationLink>
										</PaginationItem>
									))}

									<PaginationItem disabled={currentPage >= pagesCount - 1}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage + 1)}
											next
											href="#"
										/>
									</PaginationItem>
								</Pagination>
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}
