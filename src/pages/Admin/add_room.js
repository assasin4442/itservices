import React, { Component } from 'react';
import {
	Container,
	Row,
	Col,
	Button,
	FormGroup,
	Label,
	Input,
	Table,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Pagination,
	PaginationItem,
	PaginationLink
} from 'reactstrap';
import './add_location.css';
import { get, post } from '../../service/service';
import moment from 'moment';
import swal from 'sweetalert';
import { async } from 'q';

export default class add_room extends Component {
	constructor(props) {
		super(props);
		this.state = {
			modal: false,
			chr: 1,
			room: [],
			/******************** */
			cname: '',
			id: '',
			currentPage: 0
		};

		this.toggle = this.toggle.bind(this);
	}
	componentDidMount() {
		let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {
			let th = require('moment/locale/th');
			moment.updateLocale('th', th);
			this.getclassroom();
		}
	}
	getclassroom = async () => {
		let id = this.props.match.params.id;
		let res = await get('/getclassroom');
		let data = res.result.filter((e) => e.bid == id && e.status == 'open');
		this.setState({ room: data });
	};
	toggle(e) {
		if (e == 1) {
			this.setState((prevState) => ({
				modal: !prevState.modal,
				chr: 1,
				cname: ''
			}));
		} else {
			this.setState((prevState) => ({
				modal: !prevState.modal,
				chr: 2,
				cname: ''
			}));
		}
	}
	searchText(e) {
		this.getclassroom().then(() => {
			let { room } = this.state;
			let texts = e.toLowerCase();
			let a = room.filter((el) => String(el.cname).toLowerCase().indexOf(texts) > -1);
			this.setState({ room: a, currentPage: 0 });
		});
	}
	del_room = async (e) => {
		let id = e;
		swal({
			title: 'Are you sure?',
			text: 'คุณต้องการทำรายการต่อหรือไหม?',
			icon: 'warning',
			buttons: true,
			dangerMode: true
		}).then(async (willDelete) => {
			if (willDelete) {
				let res = await post('/del_room', { id: id });
				if (res.success) {
					swal('สำเร็จ!', res.message, 'success').then(() => this.getclassroom());
				} else {
					swal('ผิดพลาด!', 'ลบไม่สำเร็จ', 'error', {
						buttons: false,
						timer: 1500
					});
				}
			}
		});
	};
	insert_room = async () => {
		let { cname, room } = this.state;
		let bid = this.props.match.params.id;
		if (cname == '') {
			swal('คำเตือน!', 'กรุณากรอกหมายเลขห้องเรียน', 'warning', {
				buttons: false,
				timer: 1500
			});
		} else {
			let check = room.some((e) => e.cname == cname);
			if (check == true) {
				swal('คำเตือน!', 'หมายเลขห้องเรียนซ้ำ', 'warning', {
					buttons: false,
					timer: 2000
				});
			} else {
				let res = await post('/insert_room', { cname, bid });
				if (res.success) {
					swal('สำเร็จ!', res.message, 'success').then(() => window.location.reload());
				} else {
					swal('ผิดพลาด!', 'ผิดพลาด', 'error', {
						buttons: false,
						timer: 1500
					});
				}
			}
		}
	};
	update_room = async () => {
		let { cname, id, room } = this.state;
		let obj = {
			cname,
			id: Number(id)
		};
		let check = room.some((e) => e.cname == cname);
		if (check == true || cname == '') {
			swal('คำเตือน!', 'กรุณากรอกหมายเลขห้องเรียนใหม่', 'warning', {
				buttons: false,
				timer: 2000
			});
		} else {
			let res = await post('/update_room', obj);
			if (res.success) {
				swal('สำเร็จ!', res.message, 'success').then(() => {
					this.getclassroom();
					this.toggle(1);
				});
			} else {
				swal('ผิดพลาด!', 'ลบไม่สำเร็จ', 'error', {
					buttons: false,
					timer: 1500
				});
			}
		}
	};
	handleClick(e, index) {
		e.preventDefault();
		this.setState({
			currentPage: index
		});
	}
	render() {
		let { chr, room, cname, currentPage } = this.state;
		let pageSize = 5;
		let pagesCount = Math.ceil(room.length > 0 && room.length / pageSize);
		return (
			<div id="addlocationform" style={{ background: '#f8f8f8' }}>
				<Container style={{ paddingTop: '2rem' }}>
					<Row id="containerlocation" style={{ width: '90%' }}>
						<Col sm={12}>
							<div>
								<Label id="text1">เพิ่มห้องเรียน</Label>

								<Button outline color="secondary" href="/add_location" style={{ float: 'right' }}>
									Back
								</Button>
								<hr />
							</div>
							<Row>
								<Col sm={6}>
									<Button color="info" onClick={() => this.toggle(1)}>
										เพิ่มห้องเรียน
									</Button>
								</Col>
								<Col sm={6}>
									<Input
										type="text"
										onChange={(e) => this.searchText(e.target.value)}
										placeholder="search example"
										style={{ width: '250px', float: 'right' }}
									/>
								</Col>
							</Row>
							<Col style={{ marginTop: '1rem' }}>
								<div>
									<Table striped responsive style={{ textAlign: 'center' }}>
										<thead>
											<tr>
												<th>อาคาร</th>
												<th>หมายเลขห้องเรียน</th>
												<th style={{ width: '445px' }}>จัดการ</th>
											</tr>
										</thead>
										<tbody>
											{room.length > 0 &&
												room
													.slice(currentPage * pageSize, (currentPage + 1) * pageSize)
													.map((e) => (
														<tr>
															<td>scb{e.cname.substring(0, 1)}</td>
															<td>{e.cname}</td>
															<td>
																<Button
																	color="info"
																	style={{
																		width: '100px'
																	}}
																	size="sm"
																	onClick={() => {
																		this.toggle(2);
																		this.setState({ cname: e.cname, id: e.id });
																	}}
																>
																	แก้ไข
																</Button>
																<Button
																	color="danger"
																	size="sm"
																	style={{ width: '100px', marginLeft: '10px' }}
																	onClick={() => this.del_room(e.id)}
																>
																	ลบ
																</Button>
															</td>
														</tr>
													))}
										</tbody>
									</Table>
								</div>
							</Col>
						</Col>
						<Col sm={12}>
							<br />
							<div
								className="pagination-wrapper"
								style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
							>
								<Pagination aria-label="Page navigation example">
									<PaginationItem disabled={currentPage <= 0}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage - 1)}
											previous
											href="#"
										/>
									</PaginationItem>

									{[ ...Array(pagesCount) ].map((page, i) => (
										<PaginationItem active={i === currentPage} key={i}>
											<PaginationLink onClick={(e) => this.handleClick(e, i)} href="#">
												{i + 1}
											</PaginationLink>
										</PaginationItem>
									))}

									<PaginationItem disabled={currentPage >= pagesCount - 1}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage + 1)}
											next
											href="#"
										/>
									</PaginationItem>
								</Pagination>
							</div>
						</Col>
					</Row>
				</Container>
				{chr == 1 ? (
					<Modal isOpen={this.state.modal} toggle={() => this.toggle(1)} className={this.props.className}>
						<ModalHeader toggle={() => this.toggle(1)}>เพิ่มห้องเรียน</ModalHeader>
						<ModalBody>
							<FormGroup>
								<Label for="Na">หมายเลขห้องเรียน</Label>
								<Input value={cname} onChange={(e) => this.setState({ cname: e.target.value })} />
							</FormGroup>
						</ModalBody>
						<ModalFooter>
							<Button color="danger" onClick={() => this.toggle(1)}>
								ยกเลิก
							</Button>{' '}
							<Button color="primary" onClick={() => this.insert_room()}>
								เพิ่ม
							</Button>
						</ModalFooter>
					</Modal>
				) : (
					<Modal isOpen={this.state.modal} toggle={() => this.toggle(2)} className={this.props.className}>
						<ModalHeader toggle={() => this.toggle(2)}>แก้ไขห้องเรียน</ModalHeader>
						<ModalBody>
							<FormGroup>
								<Label for="Na">หมายเลขห้องเรียน</Label>
								<Input value={cname} onChange={(e) => this.setState({ cname: e.target.value })} />
							</FormGroup>
						</ModalBody>
						<ModalFooter>
							<Button color="danger" onClick={() => this.toggle(2)}>
								ยกเลิก
							</Button>{' '}
							<Button color="primary" onClick={this.update_room}>
								บันทึก
							</Button>
						</ModalFooter>
					</Modal>
				)}
			</div>
		);
	}
}
