import React, { Component } from 'react';
import Clock from 'react-live-clock';
import moment from 'moment-timezone';
import './css/login.css';
import { Input } from 'reactstrap';
import { FaUserAlt, FaKey } from 'react-icons/fa';
import windowSize from 'react-window-size';
import swal from 'sweetalert';
import logo from '../../assets/image/kai.png';
import { get, post } from '../../service/service';
import { Row, Col, Container } from 'reactstrap';

class Login extends Component {
	constructor(props) {
		super(props);

		this.state = {
			date: '',
			username: '',
			password: '',
			show: false,
			block_data: ''
		};
	}

	componentDidMount() {
		this.setDateTime();
	}
	setDateTime() {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		let year = +moment().format('YYYY') + 543;
		let dateString = moment().format('dddd Do MMMM') + ' ' + year;
		this.setState({ date: dateString });
		let en = require('moment/locale/en-au');
		moment.updateLocale('en-au', en);
	}

	handleKeyPress = (event) => {
		if (event.key === 'Enter') {
			this.Login();
		}
	};

	Login = async () => {
		let { username, password } = this.state;
		let obj = {
			username,
			password
		};
		if (!username || !password) {
			swal('Warning!', 'กรุณากรอก username & password', 'warning', {
				buttons: false,
				timer: 2000
			});
		} else {
			try {
				let res = await post('/login', obj);
				// console.log('res', res);
				if (res.success) {
					let result = res.result[0];
					localStorage.setItem('myData', JSON.stringify(result));
					if (result.role == 'user') {
						this.props.history.push('/home_user');
					}
					if (result.role == 'man') {
						this.props.history.push('/man_home1');
					}
					if (result.role == 'admin') {
						this.props.history.push('/admin_home');
					}
				} else {
					swal('ผิดพลาด!', res.message, 'error', {
						buttons: false,
						timer: 2000
					});
				}
			} catch (error) {
				console.log('errr', error);
			}
		}
	};
	render() {
		let { date, show } = this.state;
		let isMobile;
		this.props.windowWidth <= 767 ? (isMobile = true) : (isMobile = false);
		return (
			<div id="section-login" className="Divbody">
				<div style={{ height: '100vh' }} className="container">
					<div className="row">
						<Container style={{ paddingTop: '1rem' }}>
							<Row style={{ width: '90%', textAlign: 'center' }}>
								<Col xs={12}>
									<h1>Online appliance repair service</h1>
								</Col>
								<Col xs={12}>
									<h5>ระบบแจ้งซ่อมอุปกรณ์ออนไลน์</h5>
								</Col>
							</Row>
						</Container>
						<div
							className="col-sm"
							style={{
								display: 'flex',
								justifyContent: 'center',
								alignItems: 'center'
							}}
						>
							<div className={isMobile ? 'text-center' : 'text-right'}>
								<p className="date">
									<h5>{date}</h5>
								</p>
								<h4>
									<Clock className="timer" format={'HH:mm น.'} ticking={true} />
								</h4>
							</div>
						</div>
						<div className="col-sm">
							<div
								style={{ width: '100%' }}
								className={isMobile ? 'text-left mx-auto text-white' : 'mr-auto text-left'}
							>
								<div style={{ display: 'flex', justifyContent: 'center' }}>
									<img src={logo} style={{ width: '50px', height: '50px' }} className="App-logo" />
								</div>
								<div className="input-with-icon">
									<Input
										onChange={(e) => this.setState({ username: e.target.value })}
										bsSize="lg"
										type="text"
										placeholder="Username"
										onKeyPress={this.handleKeyPress}
									/>
									<FaUserAlt className="ic-login" />
								</div>
								<div className="input-with-icon">
									<Input
										onChange={(e) => this.setState({ password: e.target.value })}
										bsSize="lg"
										type="password"
										placeholder="Password"
										onKeyPress={this.handleKeyPress}
									/>
									<FaKey className="ic-login" />
								</div>
								<div
									onClick={this.Login}
									style={{ marginTop: '2vh', width: '100%' }}
									className="button-blue mr-auto btn-block"
								>
									เข้าสู่ระบบ
								</div>
							</div>
						</div>
					</div>
				</div>
				{/* <div className="ocean" style={{ marginTop: '10vh' }}>
					<div className="wave" />
					<div className="wave" />
				</div> */}
			</div>
		);
	}
}

export default windowSize(Login);
