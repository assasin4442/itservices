import React, { Component } from "react";

import {
  Button,
  Label,
  Row,
  Col,
  Container,
  Alert,
  Modal,
  ModalBody,
  ModalHeader,
  CardImg,
  Table,
  Card
} from "reactstrap";
import "./manre3.css";

export default class manComWork2 extends Component {
  async componentDidMount() {
    let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {}
  }
  render() {
    return (
      <div
        id="form12"
        style={{
          backgroundColor: "#f8f8f8"
        }}
      >
        <Container
          style={{
            paddingTop: "3rem"
          }}
        >
          <Row
            xs={12}
            sm={6}
            style={{
              width: "90%",
              backgroundColor: "white",
              padding: "0.30rem 0.30rem 0.30rem 0.30rem",
              borderRadius: "1rem"
            }}
          >
            <Col xs={12} sm={6}>
              <Col
                xs={12}
                sm={12}
                style={{ backgroundColor: "white", padding: "2px 2px 2px 2px" }}
              >
                <Alert color="info">รายการเบิกอุปกรณ์</Alert>
                <Row>
                  <Col sm={6}>
                    <Col>
                      <Label>ผู้ดำเนินการซ่อม</Label>
                    </Col>
                    <Col>
                      <Label
                        style={{
                          backgroundColor: "#f2f2f2",
                          width: "200px",
                          height: "2rem",
                          textAlign: "center",
                          borderRadius: "0.25rem"
                        }}
                      />
                    </Col>
                  </Col>
                  <Col sm={6}>
                    <Col>
                      <Label>วันที่</Label>
                    </Col>
                    <Col>
                      <Label
                        style={{
                          backgroundColor: "#f2f2f2",
                          width: "200px",
                          height: "2rem",
                          textAlign: "center",
                          borderRadius: "0.25rem",
                          padding: "5px 5px 5px 5px"
                        }}
                      />
                    </Col>
                  </Col>
                </Row>
                <Row>
                  <Col sm={6}>
                    <Col>
                      <Label>สถานะ</Label>
                    </Col>
                    <Col>
                      <Label
                        style={{
                          backgroundColor: "#f2f2f2",
                          width: "200px",
                          height: "2rem",
                          textAlign: "center",
                          borderRadius: "0.25rem"
                        }}
                      />
                    </Col>
                  </Col>

                  <Col sm={6}>
                    <Col>
                      <Label>ประเภท</Label>
                    </Col>
                    <Col>
                      <Label
                        style={{
                          backgroundColor: "#f2f2f2",
                          width: "200px",
                          height: "2rem",
                          textAlign: "center",
                          borderRadius: "0.25rem",
                          padding: "5px 5px 5px 5px"
                        }}
                      />
                    </Col>
                  </Col>
                </Row>
                <Row>
                  <Col sm={6}>
                    <Col>
                      <Label>สถานที่</Label>
                    </Col>
                    <Col>
                      <Label
                        style={{
                          backgroundColor: "#f2f2f2",
                          width: "200px",
                          height: "2rem",
                          textAlign: "center",
                          borderRadius: "0.25rem"
                        }}
                      />
                    </Col>
                  </Col>
                </Row>
                <hr />
                <Row>
                  <Col sm={6}>
                    <Col>
                      <Label>คำขออุปกรณ์</Label>
                    </Col>
                    <Col>
                      <Label
                        style={{
                          backgroundColor: "#f2f2f2",
                          width: "200px",
                          height: "2rem",
                          textAlign: "center",
                          borderRadius: "0.25rem"
                        }}
                      />
                    </Col>
                  </Col>

                  <Col sm={6}>
                    <Col>
                      <Label>จำนวณ</Label>
                    </Col>
                    <Col>
                      <Label
                        style={{
                          backgroundColor: "#f2f2f2",
                          width: "200px",
                          height: "2rem",
                          textAlign: "center",
                          borderRadius: "0.25rem",
                          padding: "5px 5px 5px 5px"
                        }}
                      />
                    </Col>
                  </Col>
                </Row>
                <Row>
                  <Col sm={6}>
                    <Col>
                      <Label>อุปกรณ์ที่เกิดปัญหา</Label>
                    </Col>
                    <Col>
                      <Label
                        style={{
                          backgroundColor: "#f2f2f2",
                          width: "200px",
                          height: "2rem",
                          textAlign: "center",
                          borderRadius: "0.25rem"
                        }}
                      />
                    </Col>
                  </Col>
                </Row>
                <Row>
                  <Col sm={6}>
                    <Col>
                      <Label>รายละเอียดงาน</Label>
                    </Col>
                    <Col>
                      <Label
                        style={{
                          backgroundColor: "#f2f2f2",
                          width: "200px",
                          height: "6rem",
                          textAlign: "center",
                          borderRadius: "0.25rem"
                        }}
                      />
                    </Col>
                  </Col>

                  <Col sm={6}>
                    <Col>
                      <Label>รูปภาพ</Label>
                    </Col>
                    <Col>
                      <Card inverse style={{ textAlign: "center" }}>
                        <CardImg
                          src={
                            "http://35.247.188.140/api/v1/image/repair/19.png"
                          }
                        />
                      </Card>
                    </Col>
                  </Col>
                </Row>
              </Col>
            </Col>
            <Col xs={12} sm={6}>
              <Col
                xs={12}
                style={{ backgroundColor: "white", padding: "2px 0px 0px 2px" }}
              >
                <Alert color="info">ข้อมูลผู้ใช้งาน</Alert>
                <Row>
                  <Col sm={6}>
                    <Col>
                      <Label>ชื่อ-สกุล</Label>
                    </Col>
                    <Col>
                      <Label
                        style={{
                          backgroundColor: "#f2f2f2",
                          width: "200px",
                          height: "2rem",
                          textAlign: "center",
                          borderRadius: "0.25rem"
                        }}
                      />
                    </Col>
                  </Col>

                  <Col sm={6}>
                    <Col>
                      <Label>ตำแหน่ง</Label>
                    </Col>
                    <Col>
                      <Label
                        style={{
                          backgroundColor: "#f2f2f2",
                          width: "200px",
                          height: "2rem",
                          textAlign: "center",
                          borderRadius: "0.25rem",
                          padding: "5px 5px 5px 5px"
                        }}
                      />
                    </Col>
                  </Col>
                </Row>
                <Row>
                  <Col sm={6}>
                    <Col>
                      <Label>สาขาวิชา</Label>
                    </Col>
                    <Col>
                      <Label
                        style={{
                          backgroundColor: "#f2f2f2",
                          width: "200px",
                          height: "2rem",
                          textAlign: "center",
                          borderRadius: "0.25rem"
                        }}
                      />
                    </Col>
                  </Col>

                  <Col sm={6}>
                    <Col>
                      <Label>โทรศัพท์</Label>
                    </Col>
                    <Col>
                      <Label
                        style={{
                          backgroundColor: "#f2f2f2",
                          width: "200px",
                          height: "2rem",
                          textAlign: "center",
                          borderRadius: "0.25rem",
                          padding: "5px 5px 5px 5px"
                        }}
                      />
                    </Col>
                  </Col>
                </Row>
              </Col>
              <Col
                xs={12}
                style={{
                  backgroundColor: "white",
                  padding: "2px 2px 2px 2px"
                }}
              >
                <Alert color="info">รายการเบิกอุปกรณ์</Alert>
                <Table striped responsive style={{ textAlign: "center" }}>
                  <thead>
                    <tr>
                      <td>ประเภท</td> <td>ชื่อ</td>
                      <td>จำนวน</td>
                    </tr>
                    <tr>
                      <td>111111111</td> <td>11111111</td> <td>11</td>
                    </tr>
                    <tr>
                      <td>111111111</td> <td>11111111</td> <td>11</td>
                    </tr>
                    <tr>
                      <td>111111111</td> <td>11111111</td> <td>11</td>
                    </tr>
                    <tr>
                      <td>111111111</td> <td>11111111</td> <td>11</td>
                    </tr>
                    <tr>
                      <td>111111111</td> <td>11111111</td> <td>11</td>
                    </tr>
                  </thead>
                </Table>
              </Col>
            </Col>
            <div id="btn456">
              <Row>
                <Col
                  xs={6}
                  style={{
                    textAlign: "right"
                  }}
                >
                  <Button
                    color="danger"
                    style={{
                      width: "8rem"
                    }}
                    href="/manComWork1"
                  >
                    กลับ
                  </Button>
                </Col>
              </Row>
            </div>
          </Row>
        </Container>
      </div>
    );
  }
}
