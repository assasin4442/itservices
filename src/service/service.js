// export const sever = 'http://localhost:3202/api/v1';
// export const sever = 'http://35.240.227.156/api/v1';
export const sever = 'http://itserviceudru.com/api/v1';

export const post = (path, object, token) => {
	return new Promise((resolve, reject) => {
		fetch(sever + path, {
			method: 'post',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(object),
			credentials: 'include'
		})
			.then((response) => response.json())
			.then((json) => resolve(json))
			.catch((err) => reject(err));
	});
};

export const get = (path, token) => {
	return new Promise((resolve, reject) => {
		fetch(sever + path, {
			// mode: 'include',
			method: 'get',
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'include'
		})
			.then((response) => response.json())
			.then((json) => resolve(json))
			.catch((err) => reject(err));
	});
};
