import React, { Component } from 'react';
import { Table, Button, Label, Input, Form, Row, Col, Container, FormGroup } from 'reactstrap';
import './user_repair.css';
import moment from 'moment';
import { get, post } from '../../service/service';
import swal from 'sweetalert';
let th = require('moment/locale/th');
moment.updateLocale('th', th);
export default class user_repair extends Component {
	constructor(props) {
		super(props);

		this.state = {
			date: new Date(),
			building: [],
			classroom: [],
			tool: [],
			dis: 99,
			line_token: [],
			//-----------INSERT---------
			cid: '',
			detail: '',
			device_problem: '',
			amount: ''
		};
	}
	componentDidMount = async () => {
		let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {
			this.getData();
		}
	};
	getData = async () => {
		let res = await get('/getbuilding');
		let ress = await get('/get_man');
		this.setState({
			building: res.result.filter((e) => e.status == 'open'),
			line_token: ress.result
		});
	};
	setClassroom = async (e) => {
		let res = await get('/getclassroom');
		let result = res.result.filter((el) => el.bid == e && el.status == 'open');
		this.setState({
			classroom: result
		});
	};
	submit = async () => {
		let myData = JSON.parse(localStorage.getItem('myData'));
		let { cid, device_problem, detail, amount, line_token } = this.state;
		//-----------------------------------------------------------------------
		if (cid !== '' && device_problem !== '' && detail !== '' && amount !== '') {
			let date = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			let uid = myData.id;
			let obj = {
				uid,
				cid: Number(cid),
				device_problem,
				date,
				detail,
				amount: Number(amount)
			};
			// console.log('obj', obj);
			let res = await post('/request', obj);
			if (res.success) {
				let detailJob = await post('/getrequest_by_id', { id: res.id });
				// console.log('detailJob', detailJob);
				line_token.forEach(async (e, i) => {
					try {
						let obj = {
							message: `
							ขณะนี้มีการขออุปกรณ์
							กรุณาตรวจสอบรายการ
							-----------------------
							รายละเอียดงานเบื้องต้น
							ประเภทงาน : ${detailJob.result[0].type}
							สถานที่ : scb${detailJob.result[0].cname}
							อุปกรณ์ที่ต้องการเพิ่มเติม : ${detailJob.result[0].device_problem}
							จำนวนที่ต้องการ : ${detailJob.result[0].amount}
							รายละเอียด : ${detailJob.result[0].detail}
							-----------------------
							เข้าสู่ระบบเพื่อจัดการข้อมูลเพิ่มเติมได้ที่
							http://itserviceudru.com`,
							token: e.line_token
						};
						await post('/noti_line', obj);
					} catch (error) {
						console.log('error', error);
					}
				});
				swal('สำเร็จ!', res.message, 'success', {
					buttons: false,
					timer: 1500
				}).then(() => {
					this.props.history.push('/home_user');
				});
			} else {
				swal('ผิดพลาด!', 'เพิ่มรายการไม่สำเร็จ', 'error', {
					buttons: false,
					timer: 1500
				});
			}
		} else {
			swal('ผิดพลาด!', 'กรอกข้อมูลให้ครบ', 'error', {
				buttons: false,
				timer: 1500
			});
		}
	};
	render() {
		let { date, building, classroom, dis, tool } = this.state;
		return (
			<div id="form" style={{ backgroundColor: '#f8f8f8' }}>
				<Container id="container2">
					<Row style={{ width: '90%' }}>
						<Col xs={12}>
							<div id="container1">
								<p>
									<Label style={{ fontSize: '1.5rem' }}>แจ้งขออุปกรณ์</Label>
								</p>
								<hr />
								<Label>
									วันที่ {moment(date).local('th').format('DD MMMM')}{' '}
									{Number(moment(date).local('th').format('YYYY')) + 543}
								</Label>
								<Row>
									<Col xs={12} sm={6}>
										<FormGroup>
											<Input
												type="select"
												onChange={(e) => {
													this.setState({ dis: e.target.value });
													this.setClassroom(e.target.value);
												}}
											>
												<option value={99}>เลือกอาคาร</option>
												{building.map((e, i) => <option value={e.id}>อาคาร {e.bname}</option>)}
											</Input>
										</FormGroup>
									</Col>
									<Col xs={12} sm={6}>
										<FormGroup>
											<Input
												type="select"
												disabled={dis == 99}
												onChange={(e) => this.setState({ cid: e.target.value })}
											>
												<option>เลือกห้องเรียน</option>
												{classroom.map((e, i) => <option value={e.id}>ห้อง {e.cname}</option>)}
											</Input>
										</FormGroup>
									</Col>
								</Row>
								{/* <Label>เลือกอุปกรณ์และจำนวณที่ต้องการ</Label> */}
								<Row>
									<Col xs={12} sm={6}>
										<FormGroup>
											<Input
												type="text"
												onChange={(e) => this.setState({ device_problem: e.target.value })}
												placeholder="กรอกอุปกรณ์ที่ต้องการ"
											/>
										</FormGroup>
									</Col>
									<Col xs={12} sm={6}>
										<FormGroup>
											<Input
												type="number"
												placeholder="กรอกจำนวน"
												onChange={(e) => this.setState({ amount: e.target.value })}
											/>
										</FormGroup>
									</Col>
								</Row>
								<FormGroup>
									<Label>รายละเอียดเพิ่มเติม</Label>
									<Input
										type="textarea"
										onChange={(e) => this.setState({ detail: e.target.value })}
									/>
								</FormGroup>
							</div>
						</Col>
						<div id="d">
							<Row>
								<Col style={{ textAlign: 'right' }} xs={6}>
									<Button color="danger" style={{ width: '8rem' }}>
										ยกเลิก
									</Button>
								</Col>
								<Col xs={6}>
									<Button color="success" style={{ width: '8rem' }} onClick={this.submit}>
										ยืนยัน
									</Button>
								</Col>
							</Row>
						</div>
					</Row>
				</Container>
			</div>
		);
	}
}
