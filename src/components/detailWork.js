import React, { Component } from 'react';
import {
	Button,
	Label,
	Row,
	Col,
	Container,
	Alert,
	Modal,
	ModalBody,
	ModalHeader,
	CardImg,
	Table,
	Card,
	FormGroup,
	Input
} from 'reactstrap';
import './detail.css';
import { get, post, sever } from '../service/service';
import moment from 'moment';
import swal from 'sweetalert';
import img_upload from '../assets/image/Upload.png';
import base64 from './base64';
// let api = sever+'/image/repair/';
let api = sever + '/image/repair/';

export default class detailWork extends Component {
	constructor(props) {
		super(props);

		this.state = {
			dataRepair: [],
			dataAll: [],
			role: '',
			dataDraw: [],
			//-----------------
			building: [],
			classroom: [],
			dis: 99,
			edit: false,
			device_problem: '',
			amount: '',
			detail: '',
			filebase64: '',
			cid: ''
		};
	}

	async componentDidMount() {
		let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {
			let th = require('moment/locale/th');
			moment.updateLocale('th', th);
			let USER = JSON.parse(localStorage.getItem('myData'));
			this.setState({ role: USER.role });
			let type = this.props.match.params.type;
			this.getData();
			if (type == 1) {
				this.getdataRepair();
				this.getDraw_repair();
			} else {
				this.getdataRequest();
				this.getDraw_request();
			}
		}
	}
	uploadImg = (event) => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			reader.onload = (e) => {
				this.setState({ filebase64: e.target.result });
				// console.log('e.target.result', e.target.result);
			};
			reader.readAsDataURL(event.target.files[0]);
		}
	};
	getData = async () => {
		let res = await get('/getbuilding');
		// console.log('res', res);
		this.setState({
			building: res.result.filter((e) => e.status == 'open')
		});
	};
	setClassroom = async (e) => {
		let res = await get('/getclassroom');
		let result = res.result.filter((el) => el.bid == e && el.status == 'open');
		this.setState({
			classroom: result
		});
	};
	getdataRepair = async () => {
		let res = await post('/getrepair_by_id', { id: this.props.match.params.id }); // ส่งข้อมูลไปยัง backend เพื่อเรียกใช้คำสั่ง getrepair โดยมีตัวแปลคือ id = รหัสผู้ใช้งาน และ
		console.log('getrepair_by_id', res);
		this.setState({
			// state ค่าที่ถูก  set ไว้เฉพาะ หน้าถ้าเปลี่ยนหน้าค่าที่ถูก set ไว้ก็จะหายไป
			dataAll: res.result.map((e, i) => {
				let date = moment(e.date).format('DD MMMM') + ' ' + (Number(moment(e.date).format('YYYY')) + 543);
				let time = moment(e.date).format('HH:mm');
				let cname = 'scb' + e.cname;
				return {
					rpid: e.rpid,
					date,
					time,
					cname,
					device_problem: e.device_problem,
					detail: e.detail,
					status: e.status,
					type: e.type,
					name: e.fname + ' ' + e.lname,
					position: e.position,
					branch: e.branch,
					phone: e.phone,
					detail: e.detail,
					device_problem: e.device_problem,
					bid: e.bid,
					cid: e.cid
				};
			}),
			detail: res.result[0].detail,
			amount: res.result[0].amount,
			device_problem: res.result[0].device_problem,
			cid: res.result[0].cid
		});
	};
	getdataRequest = async () => {
		let res = await post('/getrequest_by_id', { id: this.props.match.params.id });
		console.log('res', res);
		this.setState({
			dataAll: res.result.map((e, i) => {
				// dataAll = ชื่อตัวแปร
				let date = moment(e.date).format('DD MMMM') + ' ' + (Number(moment(e.date).format('YYYY')) + 543);
				let time = moment(e.date).format('HH:mm');
				let cname = 'scb' + e.cname;
				return {
					rqid: e.rqid,
					date,
					time,
					cname,
					device_problem: e.device_problem,
					detail: e.detail,
					status: e.status,
					amount: e.amount,
					type: e.type,
					name: e.fname + ' ' + e.lname,
					position: e.position,
					branch: e.branch,
					phone: e.phone,
					detail: e.detail,
					device_problem: e.device_problem,
					bid: e.bid,
					cid: e.cid
				};
			}),
			detail: res.result[0].detail,
			amount: res.result[0].amount,
			device_problem: res.result[0].device_problem,
			cid: res.result[0].cid
		});
	};
	getDraw_repair = async () => {
		let res = await post('/getdraw_repair', { did: this.props.match.params.id });
		this.setState({
			dataDraw: res.result.sort((a, b) => Number(a.tid) - Number(b.tid))
		});
	};
	getDraw_request = async () => {
		let res = await post('/getdraw_request', { did: this.props.match.params.id });
		this.setState({
			dataDraw: res.result.sort((a, b) => Number(a.tid) - Number(b.tid))
		});
	};
	updateStatus = async () => {
		let type = this.props.match.params.type;
		let USER = JSON.parse(localStorage.getItem('myData'));
		if (type == 1) {
			let id = this.props.match.params.id;
			let res = await post('/updatestatus', { id, mid: USER.id });
			if (res.success) {
				swal('สำเร็จ!', res.message, 'success', {
					buttons: false,
					timer: 1500
				}).then(() => {
					this.props.history.push('/man_home1');
				});
			} else {
				swal('ผิดพลาด!', 'ทำรายการไม่สำเร็จ', 'error', {
					buttons: false,
					timer: 1500
				});
			}
		} else {
			let id = this.props.match.params.id;
			let res = await post('/updatestatus3', { id, mid: USER.id });
			if (res.success) {
				swal('สำเร็จ!', res.message, 'success', {
					buttons: false,
					timer: 1500
				}).then(() => {
					this.props.history.push('/man_home1');
				});
			} else {
				swal('ผิดพลาด!', 'ทำรายการไม่สำเร็จ', 'error', {
					buttons: false,
					timer: 1500
				});
			}
		}
	};
	editData = async () => {
		// console.log('this.state', this.state);
		let { cid, detail, amount, device_problem, filebase64 } = this.state;
		try {
			let obj = {
				image: filebase64 !== '' ? filebase64.split(',')[1] : '',
				id: Number(this.props.match.params.id),
				cid: Number(cid),
				device_problem,
				detail,
				amount: Number(amount)
			};
			let res = await post('/update_list', obj);
			if (res.success) {
				swal('สำเร็จ!', res.message, 'success', {
					buttons: false,
					timer: 1500
				}).then(() => {
					window.location.reload();
				});
			} else {
				swal('ผิดพลาด!', 'ทำรายการไม่สำเร็จ', 'error', {
					buttons: false,
					timer: 1500
				});
			}
		} catch (error) {
			swal('ผิดพลาด!', 'ทำรายการไม่สำเร็จ', 'error', {
				buttons: false,
				timer: 1500
			});
		}
	};
	render() {
		let { type, id } = this.props.match.params;
		let {
			dataAll,
			role,
			dataDraw,
			edit,
			building,
			classroom,
			dis,
			amount,
			detail,
			device_problem,
			filebase64
		} = this.state;
		return (
			<div
				id="form12"
				style={{
					backgroundColor: '#f8f8f8'
				}}
			>
				<Container
					style={{
						paddingTop: '0.5rem'
					}}
				>
					{dataAll.length > 0 &&
						dataAll.map((e) => (
							<Row
								xs={12}
								sm={6}
								style={{
									width: '90%',
									backgroundColor: 'white',
									padding: '0.30rem 0.30rem 0.30rem 0.30rem',
									borderRadius: '1rem'
								}}
							>
								<Col xs={12} sm={6}>
									<Col
										xs={12}
										sm={12}
										style={{ backgroundColor: 'white', padding: '2px 2px 2px 2px' }}
									>
										<Alert color="info">รายการเบิกอุปกรณ์</Alert>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>วันที่</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{e.date}
													</Label>
												</Col>
											</Col>
											<Col sm={6}>
												<Col>
													<Label>เวลา</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.time}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>สถานะ</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.status}
													</Label>
												</Col>
											</Col>

											<Col sm={6}>
												<Col>
													<Label>ประเภท</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{e.type}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>สถานที่</Label>
												</Col>
												<Col>
													{edit === false ? (
														<Label
															style={{
																backgroundColor: '#f2f2f2',
																width: '200px',
																height: '2rem',
																textAlign: 'center',
																borderRadius: '0.25rem'
															}}
														>
															{e.cname}
														</Label>
													) : (
														<FormGroup>
															<Input
																type="select"
																onChange={(e) => {
																	this.setState({ dis: e.target.value });
																	this.setClassroom(e.target.value);
																}}
															>
																<option value={99}>เลือกอาคาร</option>
																{building.map((e, i) => (
																	<option value={e.id}>อาคาร {e.bname}</option>
																))}
															</Input>
														</FormGroup>
													)}
												</Col>
											</Col>
											{edit === true && (
												<Col>
													<Col>
														<Label>ห้อง</Label>
													</Col>
													<Col>
														<FormGroup>
															<Input
																type="select"
																disabled={dis == 99}
																onChange={(e) => this.setState({ cid: e.target.value })}
															>
																<option>เลือกห้องเรียน</option>
																{classroom.map((e, i) => (
																	<option value={e.id}>ห้อง {e.cname}</option>
																))}
															</Input>
														</FormGroup>
													</Col>
												</Col>
											)}
										</Row>
										<hr />
										<Row>
											<Col sm={6}>
												<Col>
													<Label>คำขออุปกรณ์</Label>
												</Col>
												<Col>
													{edit === false ? (
														<Label
															style={{
																backgroundColor: '#f2f2f2',
																width: '200px',
																height: '2rem',
																textAlign: 'center',
																borderRadius: '0.25rem'
															}}
														>
															{type == 1 ? '-' : e.device_problem}
														</Label>
													) : type == 1 ? (
														<Label
															style={{
																backgroundColor: '#f2f2f2',
																width: '200px',
																height: '2rem',
																textAlign: 'center',
																borderRadius: '0.25rem'
															}}
														>
															{type == 1 ? '-' : e.device_problem}
														</Label>
													) : (
														<Input
															type="text"
															id="c"
															value={device_problem}
															onChange={(e) =>
																this.setState({ device_problem: e.target.value })}
														/>
													)}
												</Col>
											</Col>

											<Col sm={6}>
												<Col>
													<Label>จำนวน</Label>
												</Col>
												<Col>
													{edit === false ? (
														<Label
															style={{
																backgroundColor: '#f2f2f2',
																width: '200px',
																height: '2rem',
																textAlign: 'center',
																borderRadius: '0.25rem',
																padding: '5px 5px 5px 5px'
															}}
														>
															{type == 1 ? '-' : e.amount}
														</Label>
													) : type == 1 ? (
														<Label
															style={{
																backgroundColor: '#f2f2f2',
																width: '200px',
																height: '2rem',
																textAlign: 'center',
																borderRadius: '0.25rem',
																padding: '5px 5px 5px 5px'
															}}
														>
															{type == 1 ? '-' : e.amount}
														</Label>
													) : (
														<Input
															type="text"
															id="c"
															value={amount}
															onChange={(e) => this.setState({ amount: e.target.value })}
														/>
													)}
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>อุปกรณ์ที่เกิดปัญหา</Label>
												</Col>
												<Col>
													{edit === false ? (
														<Label
															style={{
																backgroundColor: '#f2f2f2',
																width: '200px',
																height: '2rem',
																textAlign: 'center',
																borderRadius: '0.25rem'
															}}
														>
															{type == 1 ? e.device_problem : '-'}
														</Label>
													) : type == 1 ? (
														<Input
															type="text"
															id="c"
															value={device_problem}
															onChange={(e) =>
																this.setState({ device_problem: e.target.value })}
														/>
													) : (
														<Label
															style={{
																backgroundColor: '#f2f2f2',
																width: '200px',
																height: '2rem',
																textAlign: 'center',
																borderRadius: '0.25rem'
															}}
														>
															{type == 1 ? e.device_problem : '-'}
														</Label>
													)}
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>รายละเอียดงาน</Label>
												</Col>
												<Col>
													{edit === false ? (
														<Label
															style={{
																backgroundColor: '#f2f2f2',
																width: '200px',
																padding: '0.5rem',
																textAlign: 'center',
																borderRadius: '0.25rem'
															}}
														>
															{e.detail}
														</Label>
													) : (
														<Input
															type="text"
															id="c"
															value={detail}
															onChange={(e) => this.setState({ detail: e.target.value })}
														/>
													)}
												</Col>
											</Col>
										</Row>
									</Col>
								</Col>
								<Col xs={12} sm={6}>
									<Col xs={12} style={{ backgroundColor: 'white', padding: '2px 0px 0px 2px' }}>
										<Alert color="info">ข้อมูลผู้ใช้งาน</Alert>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>ชื่อ-สกุล</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.name}
													</Label>
												</Col>
											</Col>
											<Col sm={6}>
												<Col>
													<Label>ตำแหน่ง</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{e.position}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>สาขาวิชา</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.branch}
													</Label>
												</Col>
											</Col>

											<Col sm={6}>
												<Col>
													<Label>โทรศัพท์</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{e.phone}
													</Label>
												</Col>
											</Col>
										</Row>
									</Col>
									{role == 'man' &&
									e.status == 'เสร็จสิ้น' && (
										<Col
											xs={12}
											style={{
												backgroundColor: 'white',
												padding: '2px 2px 2px 2px'
											}}
										>
											<Alert color="info">รายการเบิกอุปกรณ์</Alert>
											{dataDraw.length > 0 ? (
												<Table striped responsive style={{ textAlign: 'center' }}>
													<thead>
														<tr>
															<th>ประเภท</th>
															<th>ชื่อ</th>
															<th>จำนวน</th>
														</tr>
													</thead>
													<tbody>
														{dataDraw.map((e) => (
															<tr>
																<td>{e.type == 'tool' ? 'อุปกรณ์' : 'อะไหล่'}</td>
																<td>{e.tname}</td>
																<td>{e.amount}</td>
															</tr>
														))}
													</tbody>
												</Table>
											) : (
												<text>---ไม่มีรายการเบิก---</text>
											)}
										</Col>
									)}
									{type == 1 && (
										<div>
											<Alert color="info">รูปภาพ</Alert>
											{edit === false ? (
												<Col>
													<Card inverse style={{ textAlign: 'center' }}>
														{type == 1 ? (
															<CardImg
																style={{ width: 'auto', height: '13rem' }}
																src={api + id + '.png'}
															/>
														) : (
															<CardImg
																style={{ width: 'auto', height: '13rem' }}
																src={require('../assets/image/default-image.jpg')}
															/>
														)}
													</Card>
												</Col>
											) : (
												<Col>
													{filebase64 !== '' && (
														<div
															style={{
																width: '100%',
																display: 'flex',
																justifyContent: 'center'
															}}
														>
															<img
																src={filebase64}
																style={{ width: '7.5rem', height: '7.5rem' }}
															/>
														</div>
													)}
													<br />
													<div style={{ textAlign: 'center' }}>
														<label
															style={{
																cursor: 'pointer',
																padding: '0.5rem',
																backgroundColor: '#e9ecef',
																borderRadius: '0.4rem'
															}}
														>
															<input
																type="file"
																onChange={this.uploadImg}
																accept="image/x-png,image/gif,image/jpeg"
															/>
															<img
																src={img_upload}
																style={{ width: '1rem', height: '1rem' }}
															/>
															<text>อัปโหลดรูปภาพ</text>
														</label>
													</div>
												</Col>
											)}
										</div>
									)}
								</Col>
								<div id="btn456">
									<Row>
										<Col
											xs={6}
											style={{
												textAlign: 'right'
											}}
										>
											<Button
												color="danger"
												style={{
													width: '8rem'
												}}
												onClick={() => {
													let { edit } = this.state;
													if (edit === true) {
														this.setState({ edit: false });
														this.componentDidMount();
													} else {
														this.props.history.goBack();
													}
												}}
											>
												{edit === false ? 'กลับ' : 'ยกเลิก'}
											</Button>
										</Col>
										<Col
											xs={6}
											style={{
												textAlign: 'left'
											}}
										>
											{role == 'man' ? (
												e.status !== 'เสร็จสิ้น' && (
													<Button
														color="success"
														style={{
															width: '8rem'
														}}
														onClick={() => this.updateStatus()}
													>
														ดำเนินงาน
													</Button>
												)
											) : role == 'user' ? (
												e.status == 'รอดำเนินการ' && (
													<Button
														color="success"
														style={{
															width: '8rem'
														}}
														onClick={() => {
															let { edit } = this.state;
															if (edit === true) {
																this.editData();
															} else {
																this.setState({ edit: true });
															}
														}}
													>
														{edit === false ? 'แก้ไขรายการ' : 'ยืนยัน'}
													</Button>
												)
											) : null}
										</Col>
									</Row>
								</div>
							</Row>
						))}
				</Container>
			</div>
		);
	}
}
