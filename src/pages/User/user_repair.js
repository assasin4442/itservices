import React, { Component } from 'react';
import { Table, Button, Label, Input, Form, Row, Col, Container, FormGroup } from 'reactstrap';
import './user_repair.css';
import img_upload from '../../assets/image/Upload.png';
import moment from 'moment';
import { get, post } from '../../service/service';
import swal from 'sweetalert';
import base64 from '../../components/base64';
let th = require('moment/locale/th');
moment.updateLocale('th', th);

export default class user_repair extends Component {
	constructor(props) {
		super(props);

		this.state = {
			date: new Date(),
			filebase64: '',
			dis: 99,
			building: [],
			classroom: [],
			line_token: [],
			// tool: '',
			//-----------INSERT-----------
			cid: '',
			detail: '',
			device_problem: ''
		};
	}
	componentDidMount = async () => {
		let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {
			this.getData();
		}
	};
	getData = async () => {
		let res = await get('/getbuilding');
		let ress = await get('/get_man');
		this.setState({
			building: res.result.filter((e) => e.status == 'open'),
			line_token: ress.result
		});
	};
	uploadImg = (event) => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			reader.onload = (e) => {
				this.setState({ filebase64: e.target.result });
				// console.log('e.target.result', e.target.result);
			};
			reader.readAsDataURL(event.target.files[0]);
		}
	};
	setClassroom = async (e) => {
		let res = await get('/getclassroom');
		let result = res.result.filter((el) => el.bid == e && el.status == 'open');
		this.setState({
			classroom: result
		});
	};
	submit = async () => {
		let myData = JSON.parse(localStorage.getItem('myData'));
		let { cid, device_problem, detail, filebase64, line_token } = this.state;

		if (cid !== '' && device_problem !== '' && detail !== '') {
			let date = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
			let uid = myData.id;
			let obj = {
				uid,
				cid: Number(cid),
				device_problem,
				date,
				detail,
				image: filebase64 !== '' ? filebase64.split(',')[1] : base64.img_default.split(',')[1]
			};
			// console.log('obj', obj);
			let res = await post('/repair', obj);
			if (res.success) {
				let detailJob = await post('/getrepair_by_id', { id: res.id });
				// console.log('detailJob', detailJob);
				line_token.forEach(async (e, i) => {
					try {
						let obj = {
							message: `
							ขณะนี้มีการแจ้งซ่อมอุปกรณ์
							กรุณาตรวจสอบรายการ
							-----------------------
							รายละเอียดงานเบื้องต้น
							ประเภทงาน : ${detailJob.result[0].type}
							สถานที่ : scb${detailJob.result[0].cname}
							อุปกรณ์ที่เกิดปัญหา : ${detailJob.result[0].device_problem}
							รายละเอียด : ${detailJob.result[0].detail}
							-----------------------
							เข้าสู่ระบบเพื่อจัดการข้อมูลเพิ่มเติมได้ที่
							http://itserviceudru.com`,
							token: e.line_token
						};
						await post('/noti_line', obj);
					} catch (error) {
						console.log('error', error);
					}
				});
				swal('สำเร็จ!', res.message, 'success', {
					buttons: false,
					timer: 1500
				}).then(() => {
					this.props.history.push('/home_user');
				});
			} else {
				swal('ผิดพลาด!', 'กรอกข้อมูลให้ครบ', 'error', {
					buttons: false,
					timer: 1500
				});
			}
		} else {
			swal('ผิดพลาด!', 'กรอกข้อมูลให้ครบ', 'error', {
				buttons: false,
				timer: 1500
			});
		}
	};
	render() {
		let { date, filebase64, building, classroom, dis, tool } = this.state;
		return (
			<div id="form" style={{ backgroundColor: '#f8f8f8' }}>
				<Container id="container2">
					<Row style={{ width: '90%' }}>
						<Col xs={12}>
							<div id="container1">
								<p>
									<Label style={{ fontSize: '1.5rem' }}>แจ้งซ่อมอุปกรณ์</Label>
								</p>
								<hr />
								<Label>
									วันที่ {moment(date).local('th').format('DD MMMM')}{' '}
									{Number(moment(date).local('th').format('YYYY')) + 543}
								</Label>
								<Row>
									<Col xs={12} sm={4}>
										<FormGroup>
											<Input
												type="select"
												onChange={(e) => {
													this.setState({ dis: e.target.value });
													this.setClassroom(e.target.value);
												}}
											>
												<option value={99}>เลือกอาคาร</option>
												{building.map((e, i) => <option value={e.id}>อาคาร {e.bname}</option>)}
											</Input>
										</FormGroup>
									</Col>
									<Col xs={12} sm={4}>
										<FormGroup>
											<Input
												type="select"
												disabled={dis == 99}
												onChange={(e) => this.setState({ cid: e.target.value })}
											>
												<option>เลือกห้องเรียน</option>
												{classroom.map((e, i) => <option value={e.id}>ห้อง {e.cname}</option>)}
											</Input>
										</FormGroup>
									</Col>
									<Col xs={12} sm={4}>
										<FormGroup>
											<Input
												type="text"
												onChange={(e) => this.setState({ device_problem: e.target.value })}
												placeholder="กรุณากรอกอุปกรณ์ที่เกิดปัญหา"
											/>
										</FormGroup>
									</Col>
								</Row>
								<FormGroup>
									<Label>รายละเอียดเพิ่มเติม</Label>
									<Input
										type="textarea"
										onChange={(e) => this.setState({ detail: e.target.value })}
									/>
								</FormGroup>
								{filebase64 !== '' && (
									<div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
										<img src={filebase64} style={{ width: '7.5rem', height: '7.5rem' }} />
									</div>
								)}
								<br />
								<div style={{ textAlign: 'center' }}>
									<label
										style={{
											cursor: 'pointer',
											padding: '0.5rem',
											backgroundColor: '#e9ecef',
											borderRadius: '0.4rem'
										}}
									>
										<input
											type="file"
											onChange={this.uploadImg}
											accept="image/x-png,image/gif,image/jpeg"
										/>
										<img src={img_upload} style={{ width: '1rem', height: '1rem' }} />
										<text>อัปโหลดรูปภาพ</text>
									</label>
								</div>
							</div>
						</Col>
						<div id="d">
							<Row>
								<Col xs={6} style={{ textAlign: 'right' }}>
									<Button
										color="danger"
										style={{ width: '8rem' }}
										onClick={() => this.props.history.push('/home_user')}
									>
										ยกเลิก
									</Button>
								</Col>
								<Col xs={6}>
									<Button color="success" style={{ width: '8rem' }} onClick={this.submit}>
										ยืนยัน
									</Button>
								</Col>
							</Row>
						</div>
					</Row>
				</Container>
			</div>
		);
	}
}
